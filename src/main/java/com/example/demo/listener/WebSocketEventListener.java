package com.example.demo.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import java.util.Date;

@Component
public class WebSocketEventListener {
    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        System.out.println(("Received a new web socket connection"));
        String name = event.getUser().getName();

    }

    @EventListener
    public void handleWebSocketSubscribeListener(SessionSubscribeEvent event) throws InterruptedException {
        System.out.println(("Received a new web socket subscription"));
        String name = event.getUser().getName();
        //todo Чет так нихера не работает так, сообщение на фронт не летит, как - будто сообщение перебивается контроллеровским, пропадает, в отдельном потоке вроде ок
//        messagingTemplate.convertAndSendToUser(name, "/queue/reply", name + ": text from event listener");
//        Thread.sleep(2000);
        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(5000);
                    messagingTemplate.convertAndSendToUser(name, "/queue/reply", name + ": text from event listener " + new Date());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
        String username = (String) headerAccessor.getSessionAttributes().get("username");
        if (username != null) {
            System.out.println("User Disconnected : " + username);
        }
    }
}