'use strict';
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('.connecting');
var stompClient = null;
var username = null;

var but = document.querySelector('.but');


function connect(event) {

    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, onConnected, onError);


    but.classList.add('hidden');
  event.preventDefault();
}
function onConnected() {
  // Subscribe to the Public Topic
  stompClient.subscribe('/user/queue/reply', onMessageReceived);
  // Tell your username to the server
  stompClient.send("/app/start",
      {},
      JSON.stringify({sender: username, type: 'JOIN'})
  )
  connectingElement.classList.add('hidden');
}
function onError(error) {
  connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
  connectingElement.style.color = 'red';
}
function sendMessage(event) {
  var messageContent = messageInput.value.trim();
  if(messageContent && stompClient) {
    var chatMessage = {
      sender: username,
      content: messageInput.value,
      type: 'CHAT'
    };
    stompClient.send("/app/start", {}, JSON.stringify(chatMessage));
    messageInput.value = '';
  }
  event.preventDefault();
}
function onMessageReceived(payload) {
  console.log(payload);
//  var message = JSON.parse(payload.body);
  var messageElement = document.createElement('li');

  var messageText = document.createTextNode(payload.body);

  messageElement.appendChild(messageText);
  messageArea.appendChild(messageElement);
  messageArea.scrollTop = messageArea.scrollHeight;
}

but.addEventListener('click', connect, true)
messageForm.addEventListener('submit', sendMessage, true)